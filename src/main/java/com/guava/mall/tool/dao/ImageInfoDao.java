package com.guava.mall.tool.dao;

import com.guava.mall.tool.common.base.BaseDao;
import com.guava.mall.tool.entity.ImageInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/4/1 14:29
 */
@Repository
public interface ImageInfoDao extends BaseDao<ImageInfo,Long> {

    @Query("select m from ImageInfo m where m.id in (?1)")
    List<ImageInfo> findByIds(List<Long> idList);
}
