package com.guava.mall.tool.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/4/1 16:10
 */
@Getter
@Setter
public class ImageInputDto {
    private Long id;
    private String markInfo;
}
