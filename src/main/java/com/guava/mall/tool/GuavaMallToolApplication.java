package com.guava.mall.tool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/4/1 12:03
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.guava.mall.tool.dao")
@EnableJpaAuditing
public class GuavaMallToolApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuavaMallToolApplication.class);
    }
}
