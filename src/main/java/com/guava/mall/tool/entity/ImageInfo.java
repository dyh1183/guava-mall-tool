package com.guava.mall.tool.entity;

import com.guava.mall.tool.common.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/4/1 14:27
 */
@Getter
@Setter
@Entity
@Table(name = "f_image_info")
@ToString
public class ImageInfo extends BaseEntity implements Serializable {
    String sourceUrl;
    String sourceImageName;

    String targetUrl;
    String targetImageName;
    String markInfo;

    String status;

}
