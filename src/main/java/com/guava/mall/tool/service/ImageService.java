package com.guava.mall.tool.service;

import com.guava.mall.tool.entity.ImageInfo;
import com.guava.mall.tool.model.ImageInputDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/4/1 12:57
 */
public interface ImageService {
   boolean uploadImage(MultipartFile[] file);

  Page<ImageInfo> findImageList(Pageable page);

   void deleteImages(List<Long> idList);

   void bindMarkInfo(ImageInputDto markInfo);

   String exportZipFile(List<Long> imageIdList);
}
