package com.guava.mall.tool.common.response;


import lombok.Getter;

/**
 * @Author: Deng Yunhu
 * @Date: 2020/5/26 13:45
 */
@Getter
public enum ResponseEnum {
    SUCCESS("请求成功", 20000),

    FAILED("服务异常", 50000),

    UNAUTHORIZED("资源未授权,不可访问", 40001),

    ACCESS_DENIED("权限不足,不可访问", 40003),

    IllegalParam("非法参数异常", 50001),

    SERVICE_FALL_BACK("服务异常熔断",50002),

    SERVER_EXCEPTION("远程服务异常",50003),

    SERVER_TIME_OUT("执行超时,服务熔断",50004),

    SERVER_BUSY("服务繁忙，请稍候重试",50005),

    ;

    private Integer respCode;

    private String respMsg;

    ResponseEnum(String respMsg, Integer respCode) {
        this.respCode = respCode;
        this.respMsg = respMsg;
    }

    public Integer getRespCode() {
        return respCode;
    }

    public void setRespCode(Integer respCode) {
        this.respCode = respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }
}
