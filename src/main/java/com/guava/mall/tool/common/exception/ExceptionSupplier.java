package com.guava.mall.tool.common.exception;

import cn.hutool.core.util.StrUtil;

import java.util.function.Supplier;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2020/12/16 11:17
 */
public class ExceptionSupplier {

    public static Supplier<IllegalParamException> illegalParam(String errorMsg,Object... format) {
        return () -> new IllegalParamException(StrUtil.format(errorMsg,format));
    }
}
