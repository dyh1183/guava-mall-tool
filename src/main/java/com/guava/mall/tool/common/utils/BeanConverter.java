package com.guava.mall.tool.common.utils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: DengYunHu
 * @Date: 2020/12/12 16:13
 */
public class BeanConverter {


    public static void convert(Object source, Object target, String... ignoreProperties) {
        BeanUtil.copyProperties(source, target, ignoreProperties);
    }

    public static <T> T convert(Object source, Class<? extends T> target, String... ignoreProperties) {
        T targetBean = ReflectUtil.newInstanceIfPossible(target);
        BeanUtil.copyProperties(source, targetBean, ignoreProperties);
        return targetBean;
    }

    public static <U, T> List<T> convertList(List<U> sourceList, Class<? extends T> target, String... ignoreProperties) {
        if (CollectionUtil.isEmpty(sourceList)) {
            return Collections.emptyList();
        }
        return sourceList.stream().map(source -> convert(source, target, ignoreProperties)).collect(Collectors.toList());
    }
}
