package com.guava.mall.tool.common.exception;


import com.guava.mall.tool.common.response.ResponseEnum;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2020/12/11 18:23
 */
public class ServerException extends BaseException{

    public ServerException() {
        super(ResponseEnum.FAILED);
    }

    public ServerException(String errorMsg) {
        super(ResponseEnum.FAILED.getRespCode(), errorMsg);
    }
}
