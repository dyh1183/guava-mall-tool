package com.guava.mall.tool.common.exception;


import com.guava.mall.tool.common.response.ResponseEnum;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2020/12/16 12:22
 */
public class UnAuthorizedException extends BaseException{

    public UnAuthorizedException(){
        super(ResponseEnum.UNAUTHORIZED);
    }

    public UnAuthorizedException(String errorMsg){
        super(ResponseEnum.UNAUTHORIZED.getRespCode(),errorMsg);
    }
}
