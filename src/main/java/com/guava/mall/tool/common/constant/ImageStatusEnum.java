package com.guava.mall.tool.common.constant;

import lombok.Getter;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/4/1 15:21
 */
@Getter
public enum ImageStatusEnum {

    UPLOAD_NOT_HANDLE(-10,"上传成功,未处理"),
    IMAGE_NOT_FOUND(-20,"原图路径被修改,无法找到原文件，修改失败"),
    MARK_SUCCESS(10,"修改成功"),
    MARK_FAILED(-30,"图片处理失败");
    ;

    int status;
    String desc;

    ImageStatusEnum(int status,String desc){
        this.status=status;
        this.desc=desc;
    }
}
