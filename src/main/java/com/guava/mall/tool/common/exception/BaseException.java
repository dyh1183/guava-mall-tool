package com.guava.mall.tool.common.exception;

import com.guava.mall.tool.common.response.ResponseEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2020/12/11 18:22
 */
@Getter
@Setter
public class BaseException extends RuntimeException {

    private String errorMsg;

    private Integer errorCode;

    public BaseException() {
        super();
    }

    public BaseException(ResponseEnum responseEnum) {
        this.errorMsg = responseEnum.getRespMsg();
        this.errorCode = responseEnum.getRespCode();
    }

    public BaseException(Integer errorCode, String errorMsg) {
        super(errorMsg);
        this.errorMsg = errorMsg;
        this.errorCode = errorCode;
    }

    public BaseException(String errorMsg) {
        super(errorMsg);
    }

    public BaseException(String errorMsg, Throwable throwable) {
        super(errorMsg, throwable);
    }

    public BaseException(String errorMsg, Integer errorCode) {
        super(errorMsg);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }
}
