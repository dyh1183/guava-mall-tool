package com.guava.mall.tool.common.config;

import com.guava.mall.tool.common.exception.IllegalParamException;
import com.guava.mall.tool.common.exception.ServerException;
import com.guava.mall.tool.common.response.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/1/19 14:24
 */
@Slf4j
@RestControllerAdvice
public class GlobalException {

    @ExceptionHandler(Exception.class)
    public ResponseResult exception(Exception e){
        log.error("server error -> ",e);
        return ResponseResult.failed();
    }

    @ExceptionHandler(IllegalParamException.class)
    public ResponseResult IllegalParamException(IllegalParamException illegalParamException){
        return ResponseResult.failed(illegalParamException.getErrorCode(),illegalParamException.getErrorMsg());
    }

    @ExceptionHandler(ServerException.class)
    public ResponseResult ServerException(ServerException serverException){
        return ResponseResult.failed(serverException.getErrorCode(),serverException.getErrorMsg());
    }
}
