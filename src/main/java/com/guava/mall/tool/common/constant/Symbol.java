package com.guava.mall.tool.common.constant;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/1/21 17:36
 */
public class Symbol {

    public static final String BACK_SLASH = "\\";

    public static final String SLASH = "/";

    public static final String DOT = ".";
}
