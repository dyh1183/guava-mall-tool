package com.guava.mall.tool.common.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.text.StrBuilder;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.guava.mall.tool.common.constant.ProductFileEnum;
import com.guava.mall.tool.common.constant.Symbol;
import com.guava.mall.tool.common.exception.IllegalParamException;

import java.io.File;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/1/19 11:35
 */
public class ImageFileUtils {

    public static String BASE_DIR = "upload" + File.separator;

    public static String getAbsolutePath(String path){
        String jarHome = PathUtils.getJarHome();
        return jarHome.concat(File.separator).concat(path);
    }

    public static String uriToPath(String uri){
        if (StrUtil.isBlank(uri)){
            return null;
        }
        String replace = StrUtil.replace(uri, Symbol.SLASH, File.separator);
        return PathUtils.getJarHome().concat(replace);
    }

    public static String pathToUri(String path) {
        String jarHome = PathUtils.getJarHome();
        String subPath = StrUtil.subAfter(path, jarHome, false);
        String uri = StrUtil.replace(subPath, File.separator, Symbol.SLASH);
        return uri;
    }

    public static String createFilePath(Integer fileType, String productId, String extName) {
        if (ProductFileEnum.IMAGE.getType().equals(fileType)) {
            return createImagePath(productId, extName);
        }
        if (ProductFileEnum.VIDEO.getType().equals(fileType)) {
            return createVideoPath(productId, extName);
        }
        throw new IllegalParamException("未知的文件类型");
    }

    public static String createImagePath(String productId, String extName) {
        String bashPath = StrBuilder.create(createBasePath(productId))
                .append(ProductFileEnum.IMAGE.getName())
                .toString();
        FileUtil.mkdir(bashPath);
        return StrBuilder.create(bashPath)
                .append(File.separator)
                .append(IdUtil.fastSimpleUUID())
                .append(Symbol.DOT)
                .append(extName)
                .toString();
    }

    public static String createVideoPath(String productId, String extName) {
        String bashPath = StrBuilder.create(createBasePath(productId))
                .append(ProductFileEnum.VIDEO.getName())
                .toString();
        FileUtil.mkdir(bashPath);
        return StrBuilder.create(bashPath)
                .append(File.separator)
                .append(IdUtil.fastSimpleUUID())
                .append(Symbol.DOT)
                .append(extName)
                .toString();
    }

    public static String createBasePath(String productId) {
        return StrBuilder.create(PathUtils.getJarHome())
                .append(File.separator)
                .append(BASE_DIR)
//                .append(productId)
//                .append(File.separator)
                .toString();
    }

    public static String createOSSURI(Integer fileType, Long productId, String extName) {
        StrBuilder baseURL = new StrBuilder();
        if (ProductFileEnum.IMAGE.getType().equals(fileType)) {
            return baseURL.append(productId)
                    .append(Symbol.SLASH)
                    .append(ProductFileEnum.IMAGE.getName())
                    .append(Symbol.SLASH)
                    .append(IdUtil.fastSimpleUUID())
                    .append(extName)
                    .toString();
        }
        if (ProductFileEnum.VIDEO.getType().equals(fileType)) {
            return baseURL.append(productId)
                    .append(Symbol.SLASH)
                    .append(ProductFileEnum.VIDEO.getName())
                    .append(Symbol.SLASH)
                    .append(IdUtil.fastSimpleUUID())
                    .append(extName)
                    .toString();
        }
        return IdUtil.fastSimpleUUID().concat(extName);
    }

    public static void main(String[] args) {
        String png = createVideoPath("12", "mp4");
        System.out.println(png);
    }
}
