package com.guava.mall.tool.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description: swagger文档参数信息
 * @Auth: DengYunHu
 * @Date: 2020/7/6 17:56
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "swagger")
public class SwaggerProperties {

    /**
     * 扫描的基础包
     */
    private String basePackage;

    /**
     * 标题
     */
    private String tile;

    /**
     * 描述
     */
    private String description;

    /**
     * 版本
     */
    private String version;

    /**
     * 服务地址
     */
    private String termOfServiceUrl;

    /**
     * 链接名称
     */
    private String concatName;

    /**
     * 链接地址
     */
    private String contactUrl;

    /**
     * 邮箱
     */
    private String email;
}
