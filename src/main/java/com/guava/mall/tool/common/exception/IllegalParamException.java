package com.guava.mall.tool.common.exception;


import com.guava.mall.tool.common.response.ResponseEnum;

/**
 * @Author: DengYunHu
 * @Date: 2020/12/12 16:38
 */
public class IllegalParamException extends BaseException{

    public IllegalParamException(){
        super(ResponseEnum.IllegalParam);
    }

    public IllegalParamException(String errorMsg){
        super(ResponseEnum.IllegalParam.getRespCode(),errorMsg);
    }
}
