package com.guava.mall.tool.common.exception;

import com.guava.mall.tool.common.response.ResponseEnum;
import com.guava.mall.tool.common.response.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.nio.file.AccessDeniedException;

/**
 * @Author: DengYunHu
 * @Date: 2020/12/12 16:42
 */

@Slf4j
public class BaseExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseResult exceptionHandler(Exception exception) {
        log.error("global exception ->",exception);
        return ResponseResult.failed();
    }

    @ExceptionHandler(IllegalParamException.class)
    public ResponseResult illegalParamExceptionHandler(IllegalParamException exception) {
        return ResponseResult.failed(exception.getErrorCode(), exception.getErrorMsg());
    }

    @ExceptionHandler(ServerException.class)
    public ResponseResult serverExceptionHandler(ServerException exception) {
        return ResponseResult.failed(exception.getErrorCode(), exception.getErrorMsg());
    }

    @ExceptionHandler(UnAuthorizedException.class)
    public ResponseResult unAuthorizedException(UnAuthorizedException exception){
        return ResponseResult.failed(ResponseEnum.UNAUTHORIZED);
    }

    @ExceptionHandler({AccessDeniedException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ResponseResult accessExceptionHandler(AccessDeniedException exception){
        return ResponseResult.failed(ResponseEnum.ACCESS_DENIED);
    }
}
