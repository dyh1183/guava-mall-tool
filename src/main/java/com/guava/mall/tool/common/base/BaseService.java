package com.guava.mall.tool.common.base;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * Service基类
 *
 * @Author: DengYunHu
 * @Date: 2020/12/12 16:58
 */
public interface BaseService<U> {

    BaseDao getDao();

    default void save(U entity) {
        getDao().save(entity);
    }

    default List<U> findAll() {
        return getDao().findAll();
    }

    default List<U> findAll(Specification specification) {
        return getDao().findAll(specification);
    }

    default Page<U> findAll(Pageable pageable) {
        return getDao().findAll(pageable);
    }

    default Page<U> findAll(Specification specification, Pageable pageable) {
        return getDao().findAll(specification, pageable);
    }

    default void deleteById(Long id) {
        getDao().deleteById(id);
    }
}
