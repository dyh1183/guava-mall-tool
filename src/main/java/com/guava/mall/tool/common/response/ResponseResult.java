package com.guava.mall.tool.common.response;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.IoUtil;
import com.guava.mall.tool.common.exception.ServerException;
import lombok.Getter;
import lombok.Setter;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2020/12/11 18:09
 */
@Getter
@Setter
public class ResponseResult<T> {

    private Integer respCode;
    private String respDesc;
    private T data;
    private String respTime;

    public ResponseResult() {
        respTime = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    public ResponseResult(Integer code, String desc, T data) {
        this.respCode = code;
        this.respDesc = desc;
        this.data = data;
        respTime = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    public static <T> ResponseResult<T> success(T t) {
        ResponseResult responseResult = new ResponseResult();
        responseResult.setRespCode(ResponseEnum.SUCCESS.getRespCode());
        responseResult.setRespDesc(ResponseEnum.SUCCESS.getRespMsg());
        responseResult.setData(t);
        return responseResult;
    }

    public static <T> ResponseResult<T> failed(T t) {
        ResponseResult responseResult = new ResponseResult();
        responseResult.setRespCode(ResponseEnum.FAILED.getRespCode());
        responseResult.setRespDesc(ResponseEnum.FAILED.getRespMsg());
        responseResult.setData(t);
        return responseResult;
    }

    public static ResponseResult<ResponseEnum> failed(ResponseEnum responseEnum) {
        ResponseResult responseResult = new ResponseResult();
        responseResult.setRespCode(responseEnum.getRespCode());
        responseResult.setRespDesc(responseEnum.getRespMsg());
        responseResult.setData("");
        return responseResult;
    }

    public static <T> ResponseResult<T> failed(ResponseEnum responseEnum, T t) {
        ResponseResult responseResult = new ResponseResult();
        responseResult.setRespCode(responseEnum.getRespCode());
        responseResult.setRespDesc(responseEnum.getRespMsg());
        responseResult.setData(t);
        return responseResult;
    }

    public static <T> ResponseResult<T> failed() {
        ResponseResult responseResult = new ResponseResult();
        responseResult.setRespCode(ResponseEnum.FAILED.getRespCode());
        responseResult.setRespDesc(ResponseEnum.FAILED.getRespMsg());
        responseResult.setData("");
        return responseResult;
    }

    public static <T> ResponseResult<T> failed(Integer code, String desc) {
        ResponseResult responseResult = new ResponseResult();
        responseResult.setRespCode(code);
        responseResult.setRespDesc(desc);
        return responseResult;
    }

    public static void write(HttpServletResponse response, String content) {
        response.setContentType("application/json;charset=UTF-8");
        try {
            IoUtil.writeUtf8(response.getOutputStream(), true, content);
        } catch (IOException e) {
            throw new ServerException("IO异常");
        }
    }

}
