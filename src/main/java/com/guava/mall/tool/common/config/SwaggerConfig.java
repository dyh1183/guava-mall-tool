package com.guava.mall.tool.common.config;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

/**
 * @Description: swagger接口文档配置类
 * @Auth: DengYunHu
 * @Date: 2020/7/6 17:55
 */
@Profile({"dev","test","fat"})
@Configuration
@EnableSwagger2
@EnableConfigurationProperties({SwaggerProperties.class})
@ConditionalOnProperty(name = "swagger.enable", havingValue = "true")
public class SwaggerConfig {

    @Autowired
    private SwaggerProperties properties;

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(properties.getBasePackage()))
                .paths(PathSelectors.any())
                .build();
//                .securitySchemes(security())
//                .securityContexts(securityContexts());

    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(properties.getTile())
                .description(properties.getDescription())
                .termsOfServiceUrl(properties.getTermOfServiceUrl())
                .contact(new Contact(properties.getConcatName(), properties.getContactUrl(), properties.getEmail()))
                .version(properties.getVersion())
                .build();

    }

    private List<SecurityContext> securityContexts(){
        SecurityReference reference=new SecurityReference("oauth2",scopes().toArray(new AuthorizationScope[0]));
        SecurityContext securityContext=new SecurityContext(Lists.newArrayList(reference),PathSelectors.ant("/**"));
        return Lists.newArrayList(securityContext);
    }

    private List<AuthorizationScope> scopes(){
        AuthorizationScope adminScope=new AuthorizationScope("Admin","Admin");
        AuthorizationScope webScope=new AuthorizationScope("Web","Web");
        return Lists.newArrayList(adminScope,webScope);
    }

    private List<SecurityScheme> security() {
        ResourceOwnerPasswordCredentialsGrant passwordCredentialsGrant=new ResourceOwnerPasswordCredentialsGrant("/oauth/token");
        OAuth oauth2 = new OAuthBuilder()
                .name("oauth2")
                .grantTypes(Lists.newArrayList(passwordCredentialsGrant))
                .build();
        return Lists.newArrayList(oauth2);
    }
}
