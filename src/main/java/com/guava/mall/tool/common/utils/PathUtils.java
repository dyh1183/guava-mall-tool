package com.guava.mall.tool.common.utils;

import cn.hutool.core.io.FileUtil;
import org.springframework.boot.system.ApplicationHome;

import java.io.File;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/1/19 16:48
 */
public class PathUtils {

    public static String getJarHome() {
        ApplicationHome applicationHome = new ApplicationHome(PathUtils.class);
        File source = applicationHome.getSource();
        if (FileUtil.isEmpty(source)) {
            return applicationHome.getDir().getAbsolutePath();
        }
        return source.getParentFile().toString();
    }

    public static String uploadPath() {
        String upload = getJarHome().concat(File.separator).concat("upload").concat(File.separator);
        return upload;
    }

    public static String markPath(){
        String markPath = getJarHome().concat(File.separator).concat("upload").concat(File.separator).concat("mark").concat(File.separator);
        boolean exist = FileUtil.exist(markPath);
        if (!exist){
            FileUtil.mkdir(markPath);
        }
        return markPath;
    }

    public static String downLoadPath(){
        String markPath = getJarHome().concat(File.separator).concat("upload").concat(File.separator).concat("download").concat(File.separator);
        boolean exist = FileUtil.exist(markPath);
        if (!exist){
            FileUtil.mkdir(markPath);
        }
        return markPath;
    }
}
