package com.guava.mall.tool.common.constant;

import lombok.Getter;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/1/19 11:06
 */
@Getter
public enum ProductFileEnum {

    IMAGE(10, "image"),
    VIDEO(20, "video");

    private Integer type;
    private String name;

    ProductFileEnum(Integer type, String name) {
        this.type = type;
        this.name = name;
    }
}
