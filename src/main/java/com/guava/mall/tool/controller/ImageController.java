package com.guava.mall.tool.controller;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.guava.mall.tool.common.response.ResponseResult;
import com.guava.mall.tool.common.utils.ValidationUtils;
import com.guava.mall.tool.entity.ImageInfo;
import com.guava.mall.tool.model.ImageInputDto;
import com.guava.mall.tool.service.ImageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @Description:
 * @Auth: DengYunHu
 * @Date: 2021/4/1 12:54
 */
@Api(tags = "图片处理接口")
@RestController
@RequestMapping("/image")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @PostMapping("/upload")
    @ApiOperation(value = "批量上传图片")
    public ResponseResult<Boolean> uploadImage(MultipartFile[] file){
        ValidationUtils.valid(Objects.isNull(file),"文件不能为空");
        imageService.uploadImage(file);
        return ResponseResult.success(true);
    }

    @GetMapping("/list")
    @ApiOperation(value = "查询图片列表")
    public ResponseResult<Page<ImageInfo>> findImageList(@PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable){
        Page<ImageInfo> imageList = imageService.findImageList(pageable);
        return ResponseResult.success(imageList);
    }

    @DeleteMapping("/delete")
    @ApiOperation(value = "批量删除图片")
    public ResponseResult<Boolean> deleteImages(@RequestParam("idList") List<Long> idList){
        imageService.deleteImages(idList);
        return ResponseResult.success(true);
    }

    @PostMapping("/bind/markInfo")
    @ApiOperation(value = "给图片绑定备注信息")
    public ResponseResult<String> bindMarkInfo(@RequestBody List<ImageInputDto> imageInfos){
        ValidationUtils.valid(CollectionUtil.isEmpty(imageInfos),"请求参数不能为空");
        for (ImageInputDto imageInfo : imageInfos) {
            imageService.bindMarkInfo(imageInfo);
        }
        return ResponseResult.success(StrUtil.format("处理完毕,处理结果详见状态"));
    }
}
