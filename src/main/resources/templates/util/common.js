var locationHost = window.location.hostname
var hostMap = {
    '127.0.0.1': 'http://172.30.41.202:9099',
    '172.30.138.231': 'http://172.30.41.202:9099'
}
var host = hostMap[locationHost] ? hostMap[locationHost] : 'http://' + locationHost + ':9099'

// 请求封装（json）
var requestData = function (_url, _method, _data) {
    return new Promise((resolve, reject) => {
        axios({
            method: _method,
            headers: {
                'Content-type': 'application/json'
            },
            url: host + _url,
            data: _data
        }).then((response) => {
            resolve(response)
        }).catch((error) => {
            reject(error)
        })
    })
}

// 请求封装（params）
var requestDataParams = function (_url, _method, _data) {
    return new Promise((resolve, reject) => {
        axios({
            method: _method,
            headers: {
                'Content-type': 'application/json'
            },
            url: host + _url,
            params: _data
        }).then((response) => {
            resolve(response)
        }).catch((error) => {
            reject(error)
        })
    })
}

// 请求封装（form-data）
var requestDataFile = function (_url, _method, _data) {
    let formData = new FormData() // 创建一个form对象
    for (let i in _data) {
        if (_data[i] instanceof Array) {
            // 数组
            for (const j in _data[i]) {
                formData.append(i, _data[i][j])
            }
        } else {
            formData.append(i, _data[i])
        }
    }
    return new Promise((resolve, reject) => {
        axios({
            method: _method,
            headers: {
                'Content-type': 'multipart/form-data'
            },
            url: host + _url,
            data: formData
        }).then((response) => {
            resolve(response)
        }).catch((error) => {
            reject(error)
        })
    })
}