package com.guava.mall.tool;

import com.guava.mall.tool.dao.ImageInfoDao;
import com.guava.mall.tool.entity.ImageInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ToolApplicationTests {

    @Autowired
    private ImageInfoDao infoDao;

    @Test
    void contextLoads() {
        ImageInfo info=new ImageInfo();
        info.setSourceImageName("sdf");
        info.setSourceUrl("http://localhost:12");
        info.setTargetImageName("12");
        info.setTargetUrl("http://target:12");

        infoDao.save(info);
    }

}
